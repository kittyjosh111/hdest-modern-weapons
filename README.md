
Goodbye GitLab!

**This fork has ceased development on gitlab.**

**Check out this fork instead: [codeberg branch](https://codeberg.org/herstalmarkov4219/hdest-modern-weapons)**

---

*As Tokarev turns her head, she sees a heavily-wounded Welrod walk among the enemies, standing where one of them is taking out a grenade.*

*The enemy's silhouette and Welrod slowly overlap.*

*Tokarev: Welrod...*

*Her vision is blurred by tears.*

*Welrod MkII: "Do not go gentle into that good night."*

*Tokarev: "​​Old age should burn and rave at close of day;"*

*All Tokarev can see now is Welrod. She forgets the enemy, forgets the shellfire, and forgets herself.*

*Welrod MkII: "Rage."*

*Welrod MkII: Rage against the dying of the light!*

*Tokarev lunges at the enemy who is pulling out the pin of his grenade...*

*On the tactical tablet, the pings which represent them are dark.*